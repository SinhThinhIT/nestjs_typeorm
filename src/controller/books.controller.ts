import { Body, Controller, Get, Post } from "@nestjs/common";
import CreateBookDto from "src/dto/create-book.dto";
import { BooksService } from "../services/books.services";

@Controller('books')
export default class BooksController {
  constructor(private readonly booksServices: BooksService) {}
  @Post('post')
  postBooks( @Body() books: CreateBookDto) {
    return this.booksServices.insert(books);
  }
  @Get()
  getAll() {
    return this.booksServices.getAllBooks();
  }
}