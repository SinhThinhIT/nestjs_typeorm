import { Body, Controller, Get, ParseIntPipe, Post } from '@nestjs/common';
import { UserServices } from '../services/user.service';
import CreateUserDto from '../dto/create-user.dto';

@Controller('users')
export class UserController {
  constructor(private readonly usersServices: UserServices) {}

  @Post('post')
  postUser( @Body() user: CreateUserDto) {
    return this.usersServices.insert(user);
  }
  
  @Get()
  getAll() {
    return this.usersServices.getAllUsers();
  }

  @Get('books')
  getBooks( @Body('userID', ParseIntPipe) userID: number ) {
    return this.usersServices.getBooksOfUser(userID);
  }
}