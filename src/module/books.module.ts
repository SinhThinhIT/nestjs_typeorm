import { Module } from '@nestjs/common';
import BooksController from '../controller/books.controller';
import { BooksService } from '../services/books.services';

@Module({
  imports: [],
  controllers: [BooksController],
  providers: [BooksService],
})
export default class BooksModule {}