import { Module } from '@nestjs/common';
import GenreServices from '../services/genre.services';
import GenreController from '../controller/genre.controller';
@Module({
  imports: [],
  controllers: [GenreController],
  providers: [GenreServices],
})
export default class GenreModule {}