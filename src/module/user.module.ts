import { Module } from '@nestjs/common';
import { UserController } from '../controller/user.controller';
import { UserServices } from '../services/user.service';
@Module({
  imports: [],
  controllers: [UserController],
  providers: [UserServices],
})
export class UserModule {}