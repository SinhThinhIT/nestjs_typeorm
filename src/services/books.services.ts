import BookEntity from "src/entity/book.entity";
import GenreEntity from "src/entity/genre.entity";
import UserEntity from "src/entity/user.entity";
import CreateBookDto from "src/dto/create-book.dto";

export class BooksService {

  async insert(bookDetails: CreateBookDto): Promise<BookEntity> {
    const { name , userID , genreIDs } = bookDetails;
    const book = new BookEntity();
    book.name = name;
    book.user = await UserEntity.findOne(userID) ;
    book.genres=[];
    for ( let i = 0; i < genreIDs.length ; i++)
    {
             const genre = await GenreEntity.findOne(genreIDs[i]);
             book.genres.push(genre);
    }
    await book.save();
    return book;
  }
  async getAllBooks(): Promise<BookEntity[] > {
    return BookEntity.find();
  }
}